from django.urls import path
from receipts.views import get_receipts, create_receipt, account_list, category_list, create_category, create_account


urlpatterns = [
    path("receipts/", get_receipts, name="home"),
    path('receipts/create/', create_receipt, name='create_receipt'),
    path('receipts/accounts/', account_list, name='account_list'),
    path('receipts/categories/', category_list, name='category_list'),
    path('receipts/categories/create/', create_category, name='create_category'),
    path('receipts/accounts/create/', create_account, name='create_account'),
]
